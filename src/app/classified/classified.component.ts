
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import {  ImagesService } from '../images.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {
title:string;
// Category:string;
userId:string;
id:string;
idd:string = "entertainment";
  category:string = "Loading..";
  categories:object[] = [{id:1,cat: 'business'}, {id:2,cat: 'entertainment'}, {id:3,cat: 'politics'},{id:4,cat: 'sport'}, {id:5,cat: 'tech'}];

  categoryImage:string;
  constructor(public classifyService:ClassifyService, private authService:AuthService, 
    public imagesService:ImagesService,private router: Router,private route:ActivatedRoute ) { }

   addclassify() {
     console.log(this.title);
     this.title=this.classifyService.doc;
    //  this.categories=  event.target.value;
      this.classifyService.addClassify(this.userId,this.title,this.category);
     this.router.navigate(['/classifiedsave']);
      }

  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
      console.log(this.id); 
      console.log(this.userId);   
    // this.classifyService.getclassify(this.userId,this.id).subscribe(
    //   classify => {
    //     this.title = classify.data().title;
    //   })
    
    })
  this.classifyService.classify().subscribe(
    res => {
      this.category = this.classifyService.categories[res];
      console.log(this.category);
      this.categoryImage = this.imagesService.images[res];
      console.log(this.categoryImage);
    } 
  )
  
  }

}
