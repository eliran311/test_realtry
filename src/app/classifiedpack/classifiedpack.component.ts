import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { ImagesService } from '../images.service';



@Component({
  selector: 'app-classifiedpack',
  templateUrl: './classifiedpack.component.html',
  styleUrls: ['./classifiedpack.component.css']
})
export class ClassifiedpackComponent implements OnInit {
  classify$:Observable<any>;
  userId;
  categoryImage:string;
  
  constructor(public classifyService:ClassifyService,public authService:AuthService,public imagesService:ImagesService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
    this.classify$ = this.classifyService.getClassify(this.userId);
  })

}
}
