import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../images.service';
import { ClassifyService } from '../classify.service';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-updateclassify',
  templateUrl: './updateclassify.component.html',
  styleUrls: ['./updateclassify.component.css']
})
export class UpdateclassifyComponent implements OnInit {
  

title:string;
// Category:string;
userId:string;
id:string;
category:string;
  // category:string = "Loading..";
categories:object[] = [{id:1,cat: 'business'}, {id:2,cat: 'entertainment'}, {id:3,cat: 'politics'},{id:4,cat: 'sport'}, {id:5,cat: 'tech'}];

  categoryImage:string;
  constructor(public classifyService:ClassifyService, private authService:AuthService, 
    public imagesService:ImagesService,private router: Router,private route:ActivatedRoute ) { }

  //  addclassify() {
  //    console.log(this.title);
  //    this.title=this.classifyService.doc;
  //   //  this.categories=  event.target.value;
  //     this.classifyService.addClassify(this.userId,this.title,this.category);
  //    this.router.navigate(['/classifiedsave']);
  //     }
//   updateclassify(){
//   this.classifyService.updateBook(this.userId,this.id,this.title,this.category);
//   this.router.navigate(['/classifiedsave']);
// }
  ngOnInit() {
    this.id=this.route.snapshot.params.id;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
      console.log(this.id);    
    this.classifyService.getclassify(this.id,this.userId).subscribe(
      classify => {
         this.title = classify.data().title;
        this.category=classify.data().category;
        //  this.categoryImage = this.imagesService.images[];
        console.log(this.category);
      })
    
  
})
  }

  
  
     onSubmit(){
      this.id = this.route.snapshot.params.id; 
      this.classifyService.updateClassify(this.userId,this.id,this.title,this.category);
      this.router.navigate(['/classifiedsave']);

       }   


  }
