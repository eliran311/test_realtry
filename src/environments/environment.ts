// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyBgvTea6PLmXuQ7K3d9PJC2Hh1_DNxd54M",
    authDomain: "testrealtry.firebaseapp.com",
    databaseURL: "https://testrealtry.firebaseio.com",
    projectId: "testrealtry",
    storageBucket: "testrealtry.appspot.com",
    messagingSenderId: "575861933615",
    appId: "1:575861933615:web:d1cb8aca3361815a0de8da",
    measurementId: "G-LR2V71MMZ7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
